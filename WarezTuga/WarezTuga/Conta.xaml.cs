﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using WarezTuga.Data;
using Windows.Data.Xml.Dom;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Settings Flyout item template is documented at http://go.microsoft.com/fwlink/?LinkId=273769

namespace WarezTuga
{
    public sealed partial class Conta : SettingsFlyout
    {
        public Conta()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String user=User.Text;
            String pass=Pass.Password;
            User.IsEnabled = false;
            Pass.IsEnabled = false;
            BtLogin.IsEnabled = false;
            Espera.IsActive = true;
            var task = Task.Run(async () =>
            {
                
                return await SampleDataSource.login(user, pass);
            });
            
            bool resultado = (bool)task.Result;
            if (!resultado)
                Erro.Visibility = Visibility.Visible;
            else
            {
                Erro.Visibility = Visibility.Collapsed;
                var toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastImageAndText02);
                var elements = toastXml.GetElementsByTagName("text");
                elements[0].AppendChild(toastXml.CreateTextNode("Bem-vido"));
                elements[1].AppendChild(toastXml.CreateTextNode(user));
               var toastImageElements = toastXml.GetElementsByTagName("image");
               ((XmlElement)toastImageElements[0]).SetAttribute("src", SampleDataSource.USER_AVATAR);
             //  toastImageElements[0].SetAttribute("src", "");
                var toast = new ToastNotification(toastXml);
                ToastNotificationManager.CreateToastNotifier().Show(toast);
                this.Hide(); 
                
            }
            User.IsEnabled = true;
            Pass.IsEnabled = true;
            BtLogin.IsEnabled = true;
            Espera.IsActive = false;
        }

     
    }
}
