﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace WarezTuga.Data
{
    public class Pair
    {
        public Pair(string first, string second)
        {
            this.First = first;
            this.Second = second;
        }
        public string First { set; get; }
        public string Second { set; get; }
    }
    public class HostFile
    {
        public static string ImgSock = "https://dl.dropboxusercontent.com/u/5896294/sockshare2.png";
        public static string ImgPut = "https://dl.dropboxusercontent.com/u/5896294/putlocker2.png";
        public HostFile(string link, string nome, string linkEp)
        {
            this.Nome = nome;
            this.Link = link;
            this.LinkEp = linkEp;
            if (nome.Equals("sockshare"))
                this.Foto = ImgSock;
            else
                this.Foto = ImgPut;
        }
        public string Link{ get; set; }
        public string LinkEp { get; set; }
        public string Nome{ get; set; }
        public string Foto { get; set; }

    }
    public class Episodio
    {
        public Episodio(string numero,string imagem, string link)
        {
            this.Numero = "Episodio " + numero;
            this.Imagem = imagem;
            this.Link = link;


        }
        public string Numero { get; set; }
        public string Imagem { get; set; }
        public string Link { get; set; }
    }

    public class Temporada
    {
        public Temporada(int numero, string link, string foto)
        {
            this.Numero = numero;
            this.Link = link;
            this.Title = "Temporada "+ numero;
            this.Episodios = new ObservableCollection<Episodio>();
            this.Foto = foto;
        }
        public  int Numero { get; private set; }
        public String Link { get; private set; }
        public String Title { get; private set; }
        public String Foto { get; private set; }
        public ObservableCollection<Episodio> Episodios { get; set; }
        public override string ToString()
        {
            return this.Numero.ToString();
        }

    };
    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class SampleDataItem
    {
        public static int FILME=0;
        public static int SERIE=1;
        public SampleDataItem(String uniqueId, String nome, String subtitle, String imagePath, String resumo, String content, int type)
        {
            this.UniqueId = uniqueId;
            this.Title = nome;
            this.Subtitle = subtitle;
            this.Description = resumo;
            this.ImagePath = imagePath;
            //this.Content = content;
            this.Temporadas = new ObservableCollection<Temporada>();
            this.Type = type;
        }

        public string UniqueId { get;  set; }
        public string Title { get;  set; }
        public string Subtitle/**/ { get;  set; }
        public string Description { get;  set; }
        public string ImagePath { get;  set; }
        public int Type { get; set; }
      //  public string Content/**/ { get; private set; }
        public ObservableCollection<Temporada> Temporadas/**/ { get;  set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    /// <summary>
    /// Generic group data model.
    /// </summary>
    public class SampleDataGroup
    {
        public SampleDataGroup(String uniqueId, String title, String subtitle, String imagePath, String description)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this.ImagePath = imagePath;
            this.Items = new ObservableCollection<SampleDataItem>();
        }

        public string UniqueId { get; private set; }
        public string Title { get; private set; }
        public string Subtitle { get; private set; }
        public string Description { get; private set; }
        public string ImagePath { get; private set; }
        public ObservableCollection<SampleDataItem> Items { get; private set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// SampleDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public sealed class SampleDataSource
    {
        public static string MainURL = "http://www.wareztuga.tv/";
        public static string SeriesURLP1 = "pagination.ajax.php?p=";
        public static string SeriesURLP2 = "&order=name&mediaType=series";


        public static string SeriesExibicaoURL = "pagination.ajax.php?p=1&btn=seriesrunning&mediaType=series";
        public static string LoginURL = "login.ajax.php?";
        public static string LoginUSER="username=";
        public static string LoginPASS="&password=";
        public static string Hosts = "getFilehosts.ajax.php?mediaID=";
        public static string MediaTypeEpisode = "&mediaType=episodes";
        public static string MediaTypeSeries = "&mediaType=series";
        public static string MediaTypeMovie = "&mediaType=movies";
        public static string noSub = "subs/nosubs.srt";
        public static string filmesRecentesURL = "pagination.ajax.php?p=1&order=date&mediaType=movies";
        public static string PesquisaURL = "pagination.ajax.php?p=1&order=date&words=";
        public static string ContaURL = "account.php";
        public static string USER;
        public static string PASS;
        public static string USER_AVATAR;
        public static CookieContainer cookies = new CookieContainer();
        private static async Task<string> getRequest(String URL)
        {
           
            HttpClientHandler handler = new HttpClientHandler();
            HttpClient httpClient = new HttpClient(handler);
            handler.UseCookies = true;
            Uri uri = new Uri(MainURL);
     
            handler.CookieContainer = cookies;
            httpClient.MaxResponseContentBufferSize = 256000;
            httpClient.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
            HttpResponseMessage response = await httpClient.GetAsync(URL);
           
           
            Stream receiveStream = await response.Content.ReadAsStreamAsync();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.GetEncoding("Windows-1250"));
          


           // string responseBodyAsText;
          //  responseBodyAsText = await response.Content.ReadAsStringAsync();
            return readStream.ReadToEnd(); // responseBodyAsText;
        }
        

        private static async Task<string> downloadFile(String URL)
        {

            HttpClientHandler handler = new HttpClientHandler();
            HttpClient httpClient = new HttpClient(handler);
            handler.UseCookies = true;
            Uri uri = new Uri(MainURL);
          /*  Cookie c = new Cookie("username", "crixalves");
            Cookie c2 = new Cookie("password", "bbad900e63b410d0e85bfa715b2f49d0");
            cookies.Add(uri, c);
            cookies.Add(uri, c2);*/
            handler.CookieContainer = cookies;
            httpClient.MaxResponseContentBufferSize = 524288000;
            httpClient.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
            HttpResponseMessage response = await httpClient.GetAsync(URL);


            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            return responseBodyAsText;
        }
        public static string ComputeMD5(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm("MD5");
            IBuffer buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            var res = CryptographicBuffer.EncodeToHexString(hashed);
            return res;
        }

        private static async Task<string> postRequest(String URL, FormUrlEncodedContent data)
        {
            
            HttpClientHandler handler = new HttpClientHandler();
            HttpClient httpClient = new HttpClient(handler);
            handler.UseCookies = true;
            Uri uri = new Uri(MainURL);
           /* Cookie c = new Cookie("username", "crixalves");
            Cookie c2 = new Cookie("password", "bbad900e63b410d0e85bfa715b2f49d0");
            cookies.Add(uri, c);
            cookies.Add(uri, c2);*/
            handler.CookieContainer = cookies;
            httpClient.MaxResponseContentBufferSize = 256000;
            httpClient.DefaultRequestHeaders.Add("user-agent", "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
            HttpContent content = data;// new StringContent(data, Encoding.UTF8, "application/x-www-form-urlencoded");
          //  httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
          
            HttpResponseMessage response = await httpClient.PostAsync(URL,content);
           
            string responseBodyAsText;
            responseBodyAsText = await response.Content.ReadAsStringAsync();
            return responseBodyAsText;

        }
        public static  async Task<bool> login(string utl, string pass)
        {
            String result = await getRequest(MainURL + LoginURL+LoginUSER+utl+LoginPASS+pass);
            if (result == "0")
            {
                USER = utl;
                PASS = ComputeMD5(pass);
                Uri uri = new Uri(MainURL);
                Cookie c = new Cookie("username", USER);
                Cookie c2 = new Cookie("password", PASS);
                cookies.Add(uri, c);
                cookies.Add(uri, c2);
                String account = await getRequest(MainURL + ContaURL);
                int i = account.IndexOf("class=\"avatar\">");
                account = account.Substring(i);
                i=account.IndexOf("<img");
                account = account.Substring(i);
                i = account.IndexOf("\"");
                account = account.Substring(i+1);
                i = account.IndexOf("\"");
                string img=account.Substring(0,i);
                USER_AVATAR = MainURL + img;
                SaveSettings("user", USER);
                SaveSettings("pass", PASS);
                SaveSettings("img", USER_AVATAR);
                return true;
            }
               
            return false;
        }

        public static void loadData()
        {
           
            USER = RetrieveSettings("user");
            PASS = RetrieveSettings("pass");
            Uri uri = new Uri(MainURL);
            USER_AVATAR =  RetrieveSettings("img");
            if(USER!=null)
            { 
                Cookie c = new Cookie("username", USER);
                Cookie c2 = new Cookie("password", PASS);
                cookies.Add(uri, c);
                cookies.Add(uri, c2);
            }

        }
      
        public async Task<ObservableCollection<SampleDataItem>> getAllSeries()
        {
            ObservableCollection<SampleDataItem> series = new ObservableCollection<SampleDataItem>();
            for (int j = 1; j < 40; j++)
            {
                String Series = await getRequest(MainURL + SeriesURLP1 + j + SeriesURLP2);//SeriesURL);


                int i = 0;
                while (Series.IndexOf("<a href=\"serie.php") != -1)
                {
                    i = Series.IndexOf("<a href=\"serie.php");
                    String link;
                    String foto;
                    String nome;
                    String numEpisodios;
                    String numTemporada;
                    String genero;
                    String resumo;
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("\"");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("\"");
                    link = Series.Substring(0, i);
                    Series = Series.Substring(i);
                    i = Series.IndexOf("<img src=\"");
                    Series = Series.Substring(i);
                    i = Series.IndexOf("\"");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("\"");
                    foto = Series.Substring(0, i);
                    Series = Series.Substring(i);
                    i = Series.IndexOf("<div class=\"thumb-effect2\" title=\"");
                    Series = Series.Substring(i);
                    i = Series.IndexOf("title=\"");
                    Series = Series.Substring(i);
                    i = Series.IndexOf("\"");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("\"");
                    nome = Series.Substring(0, i);
                    i = Series.IndexOf("<span>");
                    Series = Series.Substring(i);
                    i = Series.IndexOf(">");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("<");
                    numEpisodios = Series.Substring(0, i);
                    i = Series.IndexOf("<span class=\"genre\">");
                    Series = Series.Substring(i);
                    i = Series.IndexOf(">");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("<");
                    genero = Series.Substring(0, i);
                    Series = Series.Substring(i);
                    i = Series.IndexOf("<span class=\"director\">");
                    Series = Series.Substring(i);
                    i = Series.IndexOf(">");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("<");
                    numTemporada = Series.Substring(0, i);
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("<span id=\"movie-synopsis\" class=\"movie-synopsis\">");
                    Series = Series.Substring(i);
                    i = Series.IndexOf(">");
                    Series = Series.Substring(i + 1);
                    i = Series.IndexOf("<");
                    resumo = Series.Substring(0, i);
                    Series = Series.Substring(i + 1);
                    String subtitle = "Temporadas: " + numTemporada + "  Episodios: " + numEpisodios;



                    SampleDataItem s = new SampleDataItem(link, nome, subtitle, MainURL + foto, resumo, "", SampleDataItem.SERIE);

                    series.Add(s);
                }
            }
            return series;
        }

        public static  async Task<ObservableCollection<SampleDataItem>> getSeries()
        {
            return await getSeries(MainURL + SeriesExibicaoURL);//SeriesURL);   
        }

        public static async Task<ObservableCollection<SampleDataItem>> getSeries(string URL)
        {
            String Series = await getRequest(URL);//SeriesURL);
            ObservableCollection<SampleDataItem> series = new ObservableCollection<SampleDataItem>();

            int i = 0;
            while (Series.IndexOf("<a href=\"serie.php") != -1)
            {
                i = Series.IndexOf("<a href=\"serie.php");
                String link;
                String foto;
                String nome;
                String numEpisodios;
                String numTemporada;
                String genero;
                String resumo;
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                link = Series.Substring(0, i);
                Series = Series.Substring(i);
                i = Series.IndexOf("<img src=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                foto = Series.Substring(0, i);
                Series = Series.Substring(i);
                i = Series.IndexOf("<div class=\"thumb-effect2\" title=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("title=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                nome = Series.Substring(0, i);
                i = Series.IndexOf("<span>");
                Series = Series.Substring(i);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                numEpisodios = Series.Substring(0, i);
                i = Series.IndexOf("<span class=\"genre\">");
                Series = Series.Substring(i);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                genero = Series.Substring(0, i);
                Series = Series.Substring(i);
                i = Series.IndexOf("<span class=\"director\">");
                Series = Series.Substring(i);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                numTemporada = Series.Substring(0, i);
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<span id=\"movie-synopsis\" class=\"movie-synopsis\">");
                Series = Series.Substring(i);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                resumo = Series.Substring(0, i);
                Series = Series.Substring(i + 1);
                String subtitle = "Temporadas: " + numTemporada + "  Episodios: " + numEpisodios;



                SampleDataItem s = new SampleDataItem(link, nome, subtitle, MainURL + foto, resumo, "", SampleDataItem.SERIE);

                series.Add(s);
            }
            return series;

        }
        public static async Task<ObservableCollection<SampleDataGroup>> pesquisa(String pesq)
        {
            ObservableCollection<SampleDataItem> series =await  getSeries(MainURL + PesquisaURL + pesq + MediaTypeSeries);
            ObservableCollection<SampleDataItem> filmes = await getFilmes(MainURL + PesquisaURL + pesq + MediaTypeMovie);
            ObservableCollection<SampleDataGroup> groups = new ObservableCollection<SampleDataGroup>();
            SampleDataGroup groupS = new SampleDataGroup("1", "Series", "", "", "");
            for (int i = 0; i < series.Count;i++ )
            {
                groupS.Items.Add(series[i]);
            }

            SampleDataGroup groupF = new SampleDataGroup("2", "Filmes", "", "", "");
            for (int i = 0; i < filmes.Count; i++)
            {
                groupF.Items.Add(filmes[i]);
            }

            groups.Add(groupS);
            groups.Add(groupF);
            return groups;
                

        }
        public static async Task<ObservableCollection<SampleDataItem>> getFilmes(String URL)
        {
            String Series = await getRequest(URL);//SeriesURL);
            ObservableCollection<SampleDataItem> filmes = new ObservableCollection<SampleDataItem>();
            int i = 0;

            while (Series.IndexOf("<div id=\"movie") != -1)
            {
                i = Series.IndexOf("<div id=\"movie");
                Series = Series.Substring(i);
                i = Series.IndexOf("<a href=\"movie.php?");
                if (i == -1)
                    break;
                Series = Series.Substring(i);
                i = Series.IndexOf("\"movie.php?");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                string link = Series.Substring(0, i);
                i = Series.IndexOf("<img src=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                string img = Series.Substring(0, i);
                i = Series.IndexOf("title=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                string nome = Series.Substring(0, i);
                i = Series.IndexOf("class=\"genre\">");
                Series = Series.Substring(i);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                string genero = Series.Substring(0, i);

                i = Series.IndexOf("class=\"year\">");
                Series = Series.Substring(i);
                i = Series.IndexOf("</");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                string ano = Series.Substring(0, i);
                i = Series.IndexOf("class=\"movie-synopsis\"");
                Series = Series.Substring(i);

                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                string resumo = Series.Substring(0, i);


                SampleDataItem s = new SampleDataItem(MainURL + link, nome, genero + ", " + ano, MainURL + img, resumo, "", SampleDataItem.FILME);
                filmes.Add(s);
            }
            return filmes;


        }
        public async Task<ObservableCollection<SampleDataItem>> getFilmes()
        {
            string url = MainURL + filmesRecentesURL;
            return await getFilmes(url);
        }


        public static async Task<String> sockshare(HostFile hf)
        {
            try { 
            String Series = await getRequest(hf.Link);
            int i = 0;
            i = Series.IndexOf("type=\"hidden\" value=\"");
            Series = Series.Substring(i);
            i = Series.IndexOf("value=\"");
            Series = Series.Substring(i);
            i = Series.IndexOf("\"");
            Series = Series.Substring(i+1);
            i = Series.IndexOf("\"");
            string hash = Series.Substring(0,i);
            i = Series.IndexOf("\"");
            FormUrlEncodedContent data=new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("hash", hash),
                    new KeyValuePair<string, string>("confirm","Continue as Free User")
            }
                   );
            Series = await postRequest(hf.Link,data);
            i = Series.IndexOf("<a href=\"/get_file.");
            Series = Series.Substring(i);
            i = Series.IndexOf("\"");
            Series = Series.Substring(i+1);
            i = Series.IndexOf("\"");
           
         
           string link = "http://www."+hf.Nome+".com"+Series.Substring(0,i);
         
           /* Series = await downloadFile(link);*/
            return link;
            }
            catch(Exception _e)
            {
                return null;
            }
           
            
        }
        public static async void bayfiles(HostFile hf)
        {
            String Series = await getRequest(hf.Link);
            int i = 0;
        }

        public static async void vidmega(HostFile hf)
        {
            String Series = await getRequest(hf.Link);
            int i = 0;
        }

        public static Task<String> resolverServidor(HostFile hf)
        {
            if (hf.Nome.Equals("sockshare"))
                return sockshare(hf);
            else if(hf.Nome.Equals("putlocker"))
                return putlocker(hf);
            return null;
            
        }

        private static async Task<string> putlocker(HostFile hf)
        {
            try { 
            String Series = await getRequest(hf.Link);
            int i = Series.IndexOf("type=\"hidden\"");
            Series = Series.Substring(i);
            i = Series.IndexOf("value=\"");
            Series = Series.Substring(i);
            i = Series.IndexOf("\"");
            Series = Series.Substring(i+1);
            i = Series.IndexOf("\"");
            string confirm = Series.Substring(0,i);

            FormUrlEncodedContent data = new FormUrlEncodedContent(new[] {       
                    new KeyValuePair<string, string>("confirm",confirm)
            });
            Series = await postRequest(hf.Link, data);
            i = Series.IndexOf("external_share");
            Series = Series.Substring(i);
            i = Series.IndexOf("<a href=\"");
            Series = Series.Substring(i);
            i = Series.IndexOf("\"");
            Series = Series.Substring(i+1);
            i = Series.IndexOf("\"");
            string l = Series = Series.Substring(0,i);
            return l;
            }
            catch (Exception _e)
            {
                return null;
            }
        }


        public static async Task<ObservableCollection<HostFile>> getServidores(string link, string MediaType)
        {
            String Series = await getRequest(link);
            int i = 0;
            //string MediaType;
            
            if (MediaType.Equals(MediaTypeMovie))
            {
                i = Series.IndexOf("<input type=\"hidden\" id=\"mediaID\" value=\"");
                MediaType = MediaTypeMovie;
            }
            else
            {
                i = Series.IndexOf("<input type=\"hidden\" id=\"episode-selected\" value=\"");
                MediaType = MediaTypeEpisode;
            }
            Series = Series.Substring(i);
            i = Series.IndexOf("value=\"");
            Series = Series.Substring(i);
            i = Series.IndexOf("\"");
            Series = Series.Substring(i+1);
            i = Series.IndexOf("\"");
            string id = Series.Substring(0,i);
            i = Series.IndexOf("\"");
            string linkP = MainURL + Hosts + id + MediaType;
            String servers = await getRequest(linkP);
            ObservableCollection<HostFile> hf = new ObservableCollection<HostFile>();
            while (servers.IndexOf("a href=\"") != -1)
            {
                i = servers.IndexOf("a href=\"");
                servers = servers.Substring(i);
                i = servers.IndexOf("\"");
                servers = servers.Substring(i);
                i = servers.IndexOf("http");
                if (i != -1)
                {
                    servers = servers.Substring(i);
                    i = servers.IndexOf("\"");
                    string linkHost = servers.Substring(0, i);
                    i = servers.IndexOf("class=\"");
                    servers = servers.Substring(i);
                    i = servers.IndexOf("\"");
                    servers = servers.Substring(i + 1);
                    i = servers.IndexOf("\"");
                    string nome = servers.Substring(0, i);
                    i = servers.IndexOf("\"");
                    if (nome.Equals("vidmega"))
                        nome = "sockshare";
                    if (nome.Equals("putlocker") || nome.Equals("sockshare"))
                        hf.Add(new HostFile(linkHost, nome, link));
                   
                }
            }
            return hf;
        }
         public static async Task<List<TimelineMarker>> getLegendas(string link)
         {
             String Series = await getRequest(link+"&url=http");
             int i = Series.IndexOf("captionUrl:");
             Series = Series.Substring(i);
             i = Series.IndexOf("/");
             Series = Series.Substring(i+1);
             i = Series.IndexOf("'");
             string linkSub =MainURL+Series.Substring(0, i) ;
             if (linkSub.Equals(MainURL + noSub))
                 Series = "";
             else
                Series = await getRequest(linkSub);
            
             Series = Series.Replace("ă", "ã");
             Series = Series.Replace("ŕ", "à");
             Series = Series.Replace("ę", "ê");
             Series = Series.Replace("ˇ", "¡");
             Series = Series.Replace("ż", "¿");
             Series = Series.Replace("ő", "õ");
             Series = Series.Replace("Ş", "ª");

            
             List<TimelineMarker> markers = new List<TimelineMarker>();
             while (Series.IndexOf("\r\n") != -1)
             {
                 TimelineMarker marker = new TimelineMarker();
                 TimelineMarker marker2 = new TimelineMarker();
                 i = Series.IndexOf("\r\n");
                 Series = Series.Substring(i + 2);
                 i = Series.IndexOf(" -->");
                 string tempoInicial = Series.Substring(0, i);
                 i = Series.IndexOf(">");
                 Series = Series.Substring(i + 2);
                 i = Series.IndexOf("\r\n");
                 string tempoFinal = Series.Substring(0, i);
                 Series = Series.Substring(i + 2);
                 i = Series.IndexOf("\r\n\r\n");
                 if(i==-1)
                 {
                     i = Series.IndexOf("\r\n");
                 }
                 string texto = Series.Substring(0, i);
                 marker.Text = texto;
                 marker2.Text = "";
                 int a = tempoInicial.IndexOf(",");
                 tempoInicial=tempoInicial.Substring(0,a)+"."+tempoInicial.Substring(a+1);
                 try
                 {
                     marker.Time = TimeSpan.ParseExact(tempoInicial, "c", null);
                     markers.Add(marker);
                 }
                 catch (Exception e) { };
                 a = tempoFinal.IndexOf(",");
                 tempoFinal = tempoFinal.Substring(0, a) + "." + tempoFinal.Substring(a + 1);
                  try
                 {
                 marker2.Time = TimeSpan.ParseExact(tempoFinal, "c", null);
                  markers.Add(marker2);
                 }
                  catch (Exception e) { };
                  i = Series.IndexOf("\r\n\r\n");
                  if (i == -1)
                  {
                      Series = "";
                      break;
                  }
                  else
                  {

                      Series = Series.Substring(i + 4);
                  }   
                 
             }

             return markers;
         }

        public static async Task<ObservableCollection<Episodio>> getEpisodios(string link)
        {
            String Series = await getRequest(link);
            int i = 0;
            i = Series.IndexOf("<div class=\"slide-content-bg\">");
            Series = Series.Substring(i);
            ObservableCollection<Episodio> eps = new ObservableCollection<Episodio>();
            while (Series.IndexOf("<a href=\"serie.php") != -1)
            {
                i = Series.IndexOf("<a href=\"serie.php");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i+1);
                i = Series.IndexOf("\"");
                String linkE = Series.Substring(0, i);
                Series = Series.Substring(i);
                i = Series.IndexOf("<img src=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                String imagem = Series.Substring(0, i);
                i = Series.IndexOf("episode-number\">");
                if (i == -1)
                    break;
                Series = Series.Substring(i);
                i = Series.IndexOf(">");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("<");
                String numero = Series.Substring(0, i);
                eps.Add(new Episodio(numero, MainURL + imagem, MainURL+linkE));
               // getServidores(MainURL + linkE);
            }
            return eps;
        }

        public static async Task<ObservableCollection<Temporada>> getTemporadas(SampleDataItem link)
        {
            String Series = await getRequest(MainURL + link.UniqueId);
            
            int i = 0;
            i = Series.IndexOf("<div class=\"thumb serie\" title=\"");
            Series = Series.Substring(i);
            ObservableCollection<Temporada> temp = new ObservableCollection<Temporada>();
            int count = 0;
            while (Regex.IsMatch(Series, "<div id=\"season.+?\" class=\"season\"><a href=\".+?\">"))
            {
                count++;
                i = Regex.Match(Series, "<div id=\"season.+?\" class=\"season\"><a href=\".+?\">").Index;
                Series = Series.Substring(i);
                i = Series.IndexOf("<a href=\"");
                Series = Series.Substring(i);
                i = Series.IndexOf("\"");
                Series = Series.Substring(i + 1);
                i = Series.IndexOf("\"");
                temp.Add(new Temporada(count,MainURL+Series.Substring(0, i),link.ImagePath));
                
            }
          
            return temp;
           


            
        }



        private static SampleDataSource _sampleDataSource = new SampleDataSource();

        private ObservableCollection<SampleDataGroup> _groups = new ObservableCollection<SampleDataGroup>();
        private ObservableCollection<SampleDataGroup> _Allgroups = new ObservableCollection<SampleDataGroup>();
        public ObservableCollection<SampleDataGroup> Groups
        {
            get { return this._groups; }
        }
        public ObservableCollection<SampleDataGroup> AllGroups
        {
            get { return this._Allgroups; }
        }

        public static async Task<IEnumerable<SampleDataGroup>> GetGroupsAsync()
        {
            await _sampleDataSource.GetSampleDataAsync();

            return _sampleDataSource.Groups;
        }

        public static async Task<IEnumerable<SampleDataGroup>> GetSerieGroupsAsync()
        {
            await _sampleDataSource.GetSampleAllDataAsync();

            return _sampleDataSource.AllGroups;
        }

        public static async Task<SampleDataGroup> GetGroupAsync(string uniqueId)
        {
            await _sampleDataSource.GetSampleAllDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.AllGroups.Where((group) => group.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
           
            return null;
        }

        public static async Task<bool> logout()
        {
          await Windows.Storage.ApplicationData.Current.ClearAsync();
          return true;
        }
 
        public static bool SaveSettings(string key, string value)
        {
            try
            {
                var applicationData = Windows.Storage.ApplicationData.Current;
                applicationData.LocalSettings.Values[key] = value;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string RetrieveSettings(string key)
        {
            try
            {
                var applicationData = Windows.Storage.ApplicationData.Current;
               
                if (applicationData.LocalSettings.Values[key] != null)
                    return applicationData.LocalSettings.Values[key].ToString();
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static async Task<ObservableCollection<Episodio>> GetEpisodiosAsync(string link)
        {
            return await getEpisodios(link);

        }
        public static async Task<SampleDataItem> GetItemAsync(string uniqueId)
        {
            await _sampleDataSource.GetSampleDataAsync();
            // Simple linear search is acceptable for small data sets
          
            var matches = _sampleDataSource.Groups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1)
            {
                var task = Task.Run(async () =>
                {
                    return await getTemporadas(matches.First());
                });
                matches.First().Temporadas = task.Result;
                return matches.First();
            }

             matches = _sampleDataSource.AllGroups.SelectMany(group => group.Items).Where((item) => item.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1)
            {
                var task = Task.Run(async () =>
                {
                    return await getTemporadas(matches.First());
                });
                matches.First().Temporadas = task.Result;
                return matches.First();
            }
            return null;
        }
        private async Task GetSampleAllDataAsync()
        {
            if (this._Allgroups.Count != 0)
                return;
           
            Uri dataUri = new Uri("ms-appx:///DataModel/SampleData.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Groups"].GetArray();

            SampleDataGroup group = new SampleDataGroup("1", "Series", "Series", "", "");
            var task = Task.Run(async () =>
            {
                return await getAllSeries();
            });
            for (int i = 0; i < task.Result.Count(); i++)
            {
                group.Items.Add(task.Result[i]);
            }
            this.AllGroups.Add(group);

        }
        private async Task GetSampleDataAsync()
        {
            if (this._groups.Count != 0)
                return;
          //  login();
            Uri dataUri = new Uri("ms-appx:///DataModel/SampleData.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Groups"].GetArray();
            
            SampleDataGroup group = new SampleDataGroup("1","Series","Series","","");
            var task = Task.Run(async () =>
            {
                return await getSeries();
            });
            for(int i=0;i<task.Result.Count();i++)
            {
                group.Items.Add(task.Result[i]);
            }
            SampleDataGroup groupF = new SampleDataGroup("2", "Filmes", "Filmes", "", "");
            var task2 = Task.Run(async () =>
            {
                return await getFilmes();
            });
            for (int i = 0; i < task2.Result.Count(); i++)
            {
                groupF.Items.Add(task2.Result[i]);
            }
         
          
            this.Groups.Add(group);
            this.Groups.Add(groupF);
            
        }
    }
}